module Wc
using LightGraphs
function closeness_centrality2(
    g::Any,
    w::Any;
    normalize=true)

    n_v = nv(g)
    closeness = zeros(n_v)

    for u = 1:n_v
        if degree(g, u) == 0     # no need to do Dijkstra here
            closeness[u] = 0.0
        else
            d = bellman_ford_shortest_paths(g, u, w).dists
            δ = filter(x->x != typemax(x), d)
            σ = sum(δ)
            l = length(δ) - 1
            if σ > 0
                closeness[u] = l / σ

                if normalize
                    n = l / (n_v-1)
                    closeness[u] *= n
                end
            end
        end
    end
    return closeness
end
export closeness_centrality2
end