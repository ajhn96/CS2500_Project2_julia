# Project 2
# TODO List
##### Direct degree distribution``

| Unweighted | Weighted |
| :-------- | :------ |
| - In-degree | - In-degree |
| - Out-Degree | - Out-Degree |
| - Total Distribution | - Total Distribution |

##### Shortest path distribution

| Directed | Undirected |
| :-------- | :------ |
| - Weighted | - Weighted |
| - Unweighted | - Unweighted |

##### Graph diameter

| Directed | Undirected |
| :-------- | :------ |
| - Weighted | - Weighted |
| - Unweighted | - Unweighted |

##### Closeness Centrality

| Directed | Undirected |
| :-------- | :------ |
| - Weighted | - Weighted |
| - Unweighted | - Unweighted |

##### Betweenness centrality distribution

#**TBD**

##### Community Detection

- Graphic one
- Graphic two

#### CS 2500 (Algorithms)

Various graph algorithms related to finding shortest path and getting different types of information about a graph.
Graph can be of various types:
- Directed Weighted
- Directed Unweighted
- Undirected Weighted
- Undirected Unweighted

### How to get each type of graph

In a directed graph, you assume that there is an edge from the source to the target user. In an undirected graph, you ignore the direction. (If there are connections from A to B with weight w1 and connections from B to A with weight w2, in the undirected version, use the minimum of w1 and w2 for the weight between A and B). In an unweighted graph, you ignore the weights. And let all the weights be 1.

## Different Measures needed

#### Directed Degree Distribution

Create six graphics of the in-degree, out-degree, and total distributions for unweighted and 
weighted graphs when the graph is directed. The X-axis represents the degree and the Y-axis 
represents the number of vertices corresponding to the degree. Find and output vertices with 
the highest in-degree, the highest out-degree, and the highest total degree for unweighted and 
weighted graphs. If there are vertices with highest indegree /outdegree/total degree more than 
one, report all those vertices

##### Unweighted

- In-degree: the number of edges from a vertex to other vertices (the number of incoming edges)
- Out-degree: the number of edges (the number of outgoing edges)

##### Weighted degree distribution

- In-degree: the weighted sum of incoming edges
- Out-degree: the weighted sum of outgoing edges

#### Shortest Path Distribution

For all-pair of vertices, compute the shortest path length between two vertices when the graph is 
directed and undirected. Create four graphics about the shortest path length distribution of 
unweighted and weighted versions of undirected and directed graphs. The X-axis represents 
the shortest path length and the Y-axis represents the number of paths corresponding to the 
shortest path length.

##### Unweighted

##### Weighted

#### Graph Diameter

Find and output the length of the longest shortest path between two vertices for 
unweighted/weighted versions of undirected and directed graphs. 
List the pairs of vertices with the longest shortest path for unweighted/weighted versions 
of undirected and directed graphs. If there are pairs with the longest shortest path length more 
than one, report all those pairs of vertices.

##### Unweighted

##### Weighted

#### Closeness Centrality

Closeness centrality of vertex i is based on the length of the average shortest path between from vertex i to all vertices.
**Need Equation**
Create four graphics about the unweighted and weighted closeness centrality distribution when the graph is undirected and directed. The X-axis represents the closeness centrality value and the Y-axis represents the number of vertices corresponding to closeness centrality values. Find and output vertices with the highest closeness centrality values for four cases. If there are vertices with highest closeness centrality value more than one, report all those vertices

#### Betweenness Centrality Distribution

Unweighted betweenness centrality of vertex i measures the number of shortest paths that pass through i.
**a whole bunch o stuff to compute**

#### Community Detection

Using unweighted betweenness centrality of edge e for undirected graphs 
Sort the unweighted betweenness centrality of edge e in a descending order. Repeatedly remove edges with the highest unweighted betweenness centrality value 5 times (if there are edges with the same centrality value, delete all). For each iteration, recalculate the unweighted graph diameter. 
Create two graphics about the trend of diameter for undirected graphs. By removing the highest, you may get to have several disconnected components. Take the maximum among the diameters of disconnected components. The X-axis represents the unweighted betweenness centrality of edge that you are removing, the Y-axis represent the diameter. 

Reference: https://en.wikipedia.org/wiki/Girvan%E2%80%93Newman_algorithm
